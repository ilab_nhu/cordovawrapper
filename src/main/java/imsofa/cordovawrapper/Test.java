/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.cordovawrapper;

import com.google.common.base.Optional;
import com.google.common.base.Strings;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author lendle
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        // TODO code application logic here
        BufferedReader input=new BufferedReader(new InputStreamReader(System.in));
        DefaultExecutor executor=new DefaultExecutor();
        String cordovaPath=getUserCommand("Enter full path to cordova [/usr/local/bin/cordova]=>", "/usr/local/bin/cordova", input);
        CommandLine command=CommandLine.parse(cordovaPath);
        
        File tempRoot=new File(System.getProperty("java.io.tmpdir"));
        //File tempRoot=new File(".");
        File tempFolder=new File(tempRoot, ""+System.currentTimeMillis());
        tempFolder.mkdirs();
        File appFolder=new File(tempFolder, "test");
        File appWWWFolder=new File(appFolder, "www");
        File platFormFolder=new File(appFolder, "platforms");
        File androidFolder=new File(platFormFolder, "android");
        File buildFolder=new File(androidFolder, "build");
        File outputsFolder=new File(buildFolder, "outputs");
        File apkFolder=new File(outputsFolder, "apk");
        
        executor.setWorkingDirectory(tempFolder);
        
        String appId=getUserCommand("Enter APP id [test.test]=>", "test.test", input);
        String appName=getUserCommand("Enter APP name [test]=>", "test", input);
        
        command.addArgument("create");
        command.addArgument("test");
        command.addArgument(appId);
        command.addArgument(appName);
        executor.execute(command);
        
        String wwwPath=getUserCommand("Enter path to your web page folder=>", "www", input);
        FileUtils.copyDirectory(new File(wwwPath), appWWWFolder);
        
        executor.setWorkingDirectory(appFolder);
        command=CommandLine.parse(cordovaPath);
        command.addArgument("platform");
        command.addArgument("add");
        command.addArgument("android");
        executor.execute(command);
        
        command=CommandLine.parse(cordovaPath);
        command.addArgument("build");
        command.addArgument("android");
        executor.execute(command);
        
        FileUtils.copyFileToDirectory(new File(apkFolder, "android-debug.apk"), new File(".") );
        Thread.sleep(1000);
        FileUtils.deleteQuietly(tempRoot);
    }
    
    private static String getUserCommand(String instruct, String defaultValue, BufferedReader input) throws IOException{
        System.out.print(instruct);
        String ret=input.readLine();
        ret=Optional.fromNullable(Strings.emptyToNull(ret)).or(defaultValue);
        return ret;
    }
}
